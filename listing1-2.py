# Spacewalk
# Author: Abinadi
# Reference: Mission Python by Sean McManus

from email.mime import image
import pygame
import pgzrun

WIDTH = 800
HEIGHT = 600
player_x = 600
player_y = 350
toggled = False

def draw():
    screen.blit(images.backdrop, (0,0))
    screen.blit(images.ship, (130, 150))
    screen.blit(images.mars, (50,50))


def update():
    global toggled
    if keyboard.f:
        if toggled:
            pygame.display.set_mode((WIDTH, HEIGHT))
            toggled = False
        else:
            pygame.display.set_mode((WIDTH, HEIGHT), pygame.FULLSCREEN)
            toggled = True
    if keyboard.escape:
        exit()

pgzrun.go()