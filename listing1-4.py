# Spacewalk
# Author: Abinadi
# Reference: Mission Python by Sean McManus

from ast import keyword
from email.mime import image
import pygame
import pgzrun

WIDTH = 800
HEIGHT = 600
player_x = 600
player_y = 350
toggled = False

def draw():
    screen.blit(images.backdrop, (0,0))
    screen.blit(images.mars, (50,50))
    screen.blit(images.astronaut, (player_x,player_y))
    screen.blit(images.ship, (550, 300))

def game_loop():
    global player_x, player_y
    if keyboard.right:
        player_x += 5
    elif keyboard.left:
        player_x -= 5
    elif keyboard.up:
        player_y -= 5
    elif keyboard.down:
        player_y += 5

def update():
    global toggled
    if keyboard.f:
        if toggled:
            pygame.display.set_mode((WIDTH, HEIGHT))
            toggled = False
        else:
            pygame.display.set_mode((WIDTH, HEIGHT), pygame.FULLSCREEN)
            toggled = True
    if keyboard.escape:
        exit()

clock.schedule_interval(game_loop, 0.03)
pgzrun.go()#samiscool